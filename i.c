#include<string.h>
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<stdint.h>

#define ADDR_FORMAT(x)   (strtoul((x),0,0) >0  && strtoul((x),0,0)<= 0xff) 

static int opt_a=0;
static int opt_b=0;
static int opt_c=0;
static int opt_d=0;
static int opt_e=0;
static char * opt_a_arg=NULL;

static void usage(){
    fprintf(stderr,"Usage:getopt [a arg] [b] [c] [d] [e]\n");
    exit(-1);
}

inline unsigned char get_char(char *argv){
    return (unsigned char)strtoul(argv,0,0);
} 

int main(int argc,char *argv[]){
    uint64_t i = 0x8001190400000000 >> 8;

    printf("i = 0x%x\n", i);

    return 0;
}
